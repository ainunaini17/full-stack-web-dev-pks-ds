<?php

namespace App\Http\Controllers;

use App\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RoleController extends Controller
{
    public function index()
    {
        $role = Role::latest()->get();

        return response()->json([
            'success' => true,
            'message' => "Data role berhasil ditampilkan",
            'data' => $role
        ]);
    }

    public function store(Request $request)
    {
        $allRequest = $request->all();
        $validator = Validator::make($allRequest, [
            'name' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $role = Role::create([
            'name' => 'required'
        ]);

        if ($role) {
            return response()->json([
                'success' => true,
                'message' => "Data role berhasil ditambahkan",
                'data' => $role
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => "Data role gagal dibuat",
        ], 409);
    }

    public function show($id)
    {
        $role = Role::find($id);

        return response()->json([
            'success' => true,
            'message' => "Detail data berhasil ditampikan",
            'data' => $role
        ], 200);
    }

    public function update(Request $request, $id)
    {
        $allRequest = $request->all();
        $validator = Validator::make($allRequest, [
            'content' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $role = Role::find($id);

        if ($role) {
            $role->update([
                'name' => $request->name
            ]);

            return response()->json([
                'success' => true,
                'message' => "Data dengan id ' . $id . ' berhasil di update",
                'data' => $role
            ]);
        }

        return response()->json([
            'success' => false,
            'message' => "Data dengan id ' . $id . ' tidak ditemukan",
        ], 404);
    }

    public function destroy($id)
    {
        $role = Role::find($id);

        if ($role) {
            $role->delete([
                'success' => true,
                'message' => "Data role berhasil di delete",
            ], 200);
        }
        return response()->json([
            'success' => false,
            'message' => "Data dengan id ' . $id . ' tidak ditemukan",
        ], 404);
    }
}
