<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UpdatePasswordController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $allRequest = $request->all();
        $validator = Validator::make($allRequest, [
            'email' => 'required',
            'password' => 'required|confirmed|min:6'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $user = User::where('email', $request->email)->first();

        if (!$user) {
            return request()->json([
                'success' => false,
                'massage' => 'email tidak ditemukan'
            ], 400);
        }

        $user->update([
            'password' => Hash::make($request->password)

        ]);

        return request()->json([
            'success' => true,
            'massage' => 'password berhasil diubah',
            'data' => $user
        ]);
    }
}
