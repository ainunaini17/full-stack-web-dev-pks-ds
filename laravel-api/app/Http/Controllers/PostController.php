<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PostController extends Controller
{
    public function index()
    {
        $posts = Post::latest()->get();

        return response()->json([
            'success' => true,
            'message' => "Data post berhasil ditampilkan",
            'data' => $posts
        ]);
    }

    public function store(Request $request)
    {
        $allRequest = $request->all();
        $validator = Validator::make($allRequest, [
            'title' => 'required',
            'description' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $posts = Post::create([
            'title' => $request->title,
            'description' => $request->description
        ]);

        if ($posts) {
            return response()->json([
                'success' => true,
                'message' => "Data berhasil ditambahkan",
                'data' => $posts
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => "Data post gagal dibuat",
        ], 409);
    }

    public function show($id)
    {
        $posts = Post::find($id);

        return response()->json([
            'success' => true,
            'message' => "Detail data berhasil ditampikan",
            'data' => $posts
        ], 200);
    }

    public function update(Request $request, $id)
    {
        $allRequest = $request->all();
        $validator = Validator::make($allRequest, [
            'title' => 'required',
            'description' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $posts = Post::find($id);

        if ($posts) {
            $posts->update([
                'title' => $request->title,
                'description' => $request->description,
            ]);

            return response()->json([
                'success' => true,
                'message' => "Data dengan id ' . $id . ' berhasil di update",
                'data' => $posts
            ]);
        }

        return response()->json([
            'success' => false,
            'message' => "Data dengan id ' . $id . ' tidak ditemukan",
        ], 404);
    }

    public function destroy($id)
    {
        $posts = Post::find($id);

        if ($posts) {
            $posts->delete([
                'success' => true,
                'message' => "Data post berhasil di delete",
            ], 200);
        }
        return response()->json([
            'success' => false,
            'message' => "Data dengan id ' . $id . ' tidak ditemukan",
        ], 404);
    }
}
