<?php

namespace App\Http\Controllers;

use App\Comment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CommentController extends Controller
{
    public function index()
    {
        $comment = Comment::latest()->get();

        return response()->json([
            'success' => true,
            'message' => "Data comment berhasil ditampilkan",
            'data' => $comment
        ]);
    }

    public function store(Request $request)
    {
        $allRequest = $request->all();
        $validator = Validator::make($allRequest, [
            'content' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $comment = Comment::create([
            'content' => 'required'
        ]);

        if ($comment) {
            return response()->json([
                'success' => true,
                'message' => "Data comment berhasil ditambahkan",
                'data' => $comment
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => "Data comment gagal dibuat",
        ], 409);
    }

    public function show($id)
    {
        $comment = Comment::find($id);

        return response()->json([
            'success' => true,
            'message' => "Detail data berhasil ditampikan",
            'data' => $comment
        ], 200);
    }

    public function update(Request $request, $id)
    {
        $allRequest = $request->all();
        $validator = Validator::make($allRequest, [
            'content' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $comment = Comment::find($id);

        if ($comment) {
            $comment->update([
                'content' => $request->content
            ]);

            return response()->json([
                'success' => true,
                'message' => "Data dengan id ' . $id . ' berhasil di update",
                'data' => $comment
            ]);
        }

        return response()->json([
            'success' => false,
            'message' => "Data dengan id ' . $id . ' tidak ditemukan",
        ], 404);
    }

    public function destroy($id)
    {
        $comment = Comment::find($id);

        if ($comment) {
            $comment->delete([
                'success' => true,
                'message' => "Data comment berhasil di delete",
            ], 200);
        }
        return response()->json([
            'success' => false,
            'message' => "Data dengan id ' . $id . ' tidak ditemukan",
        ], 404);
    }
}
