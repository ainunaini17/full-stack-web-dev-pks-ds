<?php
trait hewan
{
    public $nama;
    public $darah = 50;
    public $jumlahKaki;
    public $keahlian;

    public function __construct($nama, $jumlahKaki, $keahlian)
    {
        $this->nama = $nama;
        $this->keahlian = $keahlian;
        $this->jumlahKaki = $jumlahKaki;
    }

    public function atraksi()
    {
        return $this->nama . " memiliki " . $this->jumlahKaki . " kaki dan memiliki keahlian " . $this->keahlian;
    }
}

trait fight
{
    public $attackPower;
    public $defencePower;

    // public function __construct($attackPower, $defencePower)
    // {
    //     $this->attackPower = $attackPower;
    //     $this->defencePower = $defencePower;
    // }

    public function serang()
    {
        echo "sedang menyerang";
    }

    public function diserang($attackPower, $defencePower)
    {
        return $this->darah - $attackPower / $defencePower;
    }
}

class Elang
{
    use hewan, fight;
}

class Harimau
{
    use hewan, fight;
}

$obj = new Elang("Elang", 2, "Terbang tinggi");
echo $obj->atraksi();
echo "<br>";
echo $obj->serang();
echo "<br>";
echo $obj->diserang(50, 10, 5);
echo "<br><br>";


$obj2 = new Harimau("Harimau", 4, "Lari cepat");
echo $obj2->atraksi();
echo "<br>";
echo $obj2->serang();
echo "<br>";
echo $obj2->diserang(50, 7, 8);
echo "<br>";
