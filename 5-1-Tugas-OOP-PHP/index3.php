<?php

class Binatang
{
    public $nama;
    public function __construct($nama)
    {
        $this->nama = $nama;
    }
}

class Komodo extends Binatang
{
    public function getNama()
    {
        return $this->nama;
    }
}

$komodo = new Komodo('komodo');
echo $komodo->getNama();
