<?php

abstract class Hewan
{
    public $nama;
    public $darah = 50;
    public $jumlahKaki;
    public $keahlian;
    public function __construct($nama, $jumlahKaki, $keahlian)
    {
        $this->nama = $nama;
        $this->jumlahKaki = $jumlahKaki;
        $this->keahlian = $keahlian;
    }

    abstract public function atraksi();
}

abstract class Fight extends Hewan
{
    public $attackPower;
    public $defencePower;

    public function __construct($attackPower, $defencePower)
    {
        $this->attackPower = $attackPower;
        $this->defencePower = $defencePower;
    }

    abstract public function serang();
    abstract public function diserang();
}

class Elang extends Fight
{
    public $jumlahKaki = 2;
    public $keahlian = "terbang tinggi";
    public $attackPower = 10;
    public $defencePower = 5;
    public function __construct($nama)
    {
        $this->nama = $nama;
    }
    public function atraksi()
    {
        return $this->nama . " memiliki " . $this->jumlahKaki . " kaki dan memiliki keahlian " . $this->keahlian;
    }

    public function serang()
    {
        return "sedang menyerang";
    }

    public function diserang()
    {
        return $this->darah - $this->attackPower / $this->defencePower;
    }
}

class Harimau extends Fight
{
    public $jumlahKaki = 4;
    public $keahlian = "lari cepat";
    public $attackPower = 7;
    public $defencePower = 8;
    public function __construct($nama)
    {
        $this->nama = $nama;
    }
    public function atraksi()
    {
        return $this->nama . " memiliki " . $this->jumlahKaki . " kaki dan memiliki keahlian " . $this->keahlian;
    }

    public function serang()
    {
        return "sedang menyerang";
    }

    public function diserang()
    {
        return $this->darah - $this->attackPower / $this->defencePower;
    }
}

$elang = new Elang("Elang");
echo $elang->atraksi();
echo "<br>";
echo $elang->serang();
echo "<br>";
echo $elang->diserang();
echo "<br><br>";

$harimau = new Harimau("Harimau");
echo $harimau->atraksi();
echo "<br>";
echo $harimau->serang();
echo "<br>";
echo $harimau->diserang();
echo "<br><br>";
